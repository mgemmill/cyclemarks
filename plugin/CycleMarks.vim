" ============================================================================
" File:        CycleMarks.vim
" Version:     0.1
" Description: vim global plugin that provides simplified mark setting and
"              navigation
" Maintainer:  Mark Gemmill <mark.gemmill at gmail dot com>
" Last Change: 4 February, 2012
" License:     This program is free software. It comes without any warranty,
"              to the extent permitted by applicable law. You can redistribute
"              it and/or modify it under the terms of the Do What The Fuck You
"              Want To Public License, Version 2, as published by Sam Hocevar.
"              See http://sam.zoy.org/wtfpl/COPYING for more details.
" ============================================================================

if exists( "loaded_cycle_marks" ) 
    finish 
endif
if v:version < 700
    echoerr "CycleMarks: this plugin requires vim >= 7."
    finish
endif
let loaded_cycle_marks = 1

if !exists('g:cycle_marks_enable') | let g:cycle_marks_enable = 1 | endif
if g:cycle_marks_enable == 0 | finish | endif

if !exists('g:cycle_marks_set_mark_default') 
    let g:cycle_marks_set_mark_default = 1 
endif

" the list of acceptable marks for CycleMarks 
let s:valid_marks = split("a b c d e f g h i j k l m n o p q r s t u v w x y z")

if !exists('g:cycle_marks_mark_range')
    let g:cycle_marks_mark_range = join(s:valid_marks, "") 
endif

let s:all_marks = [] 

" Set the range of marks to by used by CycleMarks.
" Only lower case marks in the range of 'a-z" are 
" accepted. Upper case values will be lower cased and
" anything else ignored.
let g:cycle_marks_mark_range = tolower(g:cycle_marks_mark_range) 
for i in range(0, len(g:cycle_marks_mark_range) - 1)
    let mark_ = g:cycle_marks_mark_range[i]
    if index(s:all_marks, mark_) == -1 && index(s:valid_marks, mark_) >= 0
        "set as valid mark
        call add(s:all_marks, mark_)
    endif
endfor

if len(s:all_marks) == 0
    echoerr "g:cycle_marks_mark_range contained no valid marks!\r" .
          \ "reverting to default g:cycle_marks_mark_range"
    let s:all_marks = copy(s:valid_marks) 
endif

"
" FUNCTION: Sorted(list, bool)
"
" Returns a copy of the list sorted in ascending or descending order.
"
" Args: 
"
"   list_: a list of integers or strings - in our case, marks or line numbers.
"
"   ascending: either 0 (sort descending order) or 1 (sort ascending order) 
"
" N.B.
"
" Vim's `sort' function is a bit screwed up if you're sorting numbers. By
" default it converts everything to strings for comparison purposeslt. Thus:
"    :echo sort([5, 20, 16, 3])
" will arrange your list to [16, 20, 3, 5]. Try it! 
"
" To overcome this you need to provide a comparator function to `sort'
" that overrides that default 'compare all variables as strings'.
"
" Now, if you want to do a reverse sort, you can use the `reverse' 
" function, which DOES NOT compare all variables as strings. Oh, joy.
"
" As I would just rather do a 'sort([16, 20, 3, 5], asc)' or
" 'sort([16, 20, 3, 5], desc)', I've done so with the following fuctions.
"
function! Sorted(list_, ascending)
    let compare = a:ascending ? "CompareAscending" : "CompareDescending"
    let new_list = deepcopy(a:list_)
    call sort(new_list, function(compare))
    return new_list
endfunction
"
" FUNCTION: CompareAscending(var, var)
" Used by the Sorted function
"
function! CompareAscending(itemA, itemB)
    return a:itemA == a:itemB ? 0 : a:itemA > a:itemB ? 1 : -1 
endfunction
" 
" FUNCTION: CompareDescending(var, var)
" Used by the Sorted function
"
function! CompareDescending(itemA, itemB)
    return a:itemA == a:itemB ? 0 : a:itemA > a:itemB ? -1 : 1 
endfunction
"
" FUNCTION: greater-than operation
"
function! GreaterThan(x, y)
    return a:x > a:y
endfunction
"
" FUNCTION: less-than operation
"
function! LessThan(x, y)
    return a:x < a:y
endfunction

"============================================================================ 
"CLASS: MarkObject
"============================================================================ 
"
" FUNCTION: MarkObject.exists()
" Convenience method to determine if the mark is legit.
"
function MarkObject_exists() dict 
    return self.id != "" && self.line > 0
endfunction
" 
" FUNCTION: MarkObject()
" MarkObject contructor.
" Returns a new MarkObject
"
function! MarkObject(mark_, lineno)
    let mark_object = {
            \ "id": a:mark_,
            \ "line": a:lineno,
            \ "exists": function("MarkObject_exists")}
    return mark_object
endfunction

" ============================================================================ 
" CLASS: CycleMarks
" Each buffer has an instance of a CycleMarks object. CycleMarks manages a 
" list of available marks, keeping track of those that have been set and
" those that remain available.
"
" ============================================================================ 
"
" FUNCTION: CycleMarks.reset()
" Resets the buffers marks. This is only necessary when deleting all marks.
"
function CycleMarks_reset() dict
    let self.mark_pool = copy(s:all_marks)
    let self.line_lookup = {}
    let self.mark_lookup = {}
endfunction
"
" FUNCTION: CycleMarks.delete_mark(str)
" Delete internal reference to mark.
"
function CycleMarks_delete_mark(mark_) dict
    let lineno = self.line_lookup[a:mark_]
    call remove(self.mark_lookup, lineno)
    call remove(self.line_lookup, a:mark_)
    call add(self.mark_pool, a:mark_)
endfunction
"
" FUNCTION: CycleMarks.from_line(int)
" Return a MarkObject for the given line.
"
function CycleMarks_from_line(lineno) dict
    let mark_ = get(self.mark_lookup, a:lineno, "") 
    return MarkObject(mark_, a:lineno) 
endfunction
"
" FUNCTION: CycleMarks.from_mark(str)
" Returns a MarkObject for the given mark.
" XXX: this may not be in use
"
function CycleMarks_from_mark(mark_) dict
    let lineno = get(self.line_lookup, a:mark_, -1)
    return MarkObject(a:mark_, lineno)
endfunction
"
" FUNCTION: CycleMarks.has_mark(str)
" Return true if mark is set
"
function CycleMarks_has_mark(mark_) dict
    return has_key(self.line_lookup, a:mark_)
endfunction
"
" FUNCTION: CycleMarks.lines()
" Return the list of mark lines
"
function CycleMarks_lines() dict
    let lines = []
    for line in keys(self.mark_lookup)
        call add(lines, eval(line))
    endfor
    return lines
endfunction
"
" FUNCTION: CycleMarks.marks()
" Return the list of marks
"
function CycleMarks_marks() dict
    return keys(self.line_lookup)
endfunction
" 
" FUNCTION: CycleMarks.reset_mark(int, str)
" Works on the assumption that a:mark_ is a valid mark
" and is in the `current_mark` list.
"
function CycleMarks_reset_mark(lineno, mark_) dict
    if !has_key(self.line_lookup, a:mark_)
        return
    endif
    let current_line = self.line_lookup[a:mark_]
    if current_line == a:lineno " mark has not changed 
        return
    endif
    " remove any line lookups that match other marks
    " these will be refreshed with subsequent resets
    if has_key(self.mark_lookup, a:lineno)
        call remove(self.mark_lookup, a:lineno)
    endif
    " replace the current mark lookup 
    if has_key(self.mark_lookup, current_line)
        call remove(self.mark_lookup, current_line)
    endif
    let self.mark_lookup[a:lineno] = a:mark_

    "fix line_lookup
    let self.line_lookup[a:mark_] = a:lineno
endfunction
" 
"  FUNCTION: CycleMarks.set_mark(int, str)
"  Explicitly set a mark, without regard for
"  existing marks settings. 
"
function CycleMarks_set_mark(lineno, mark_) dict
    if has_key(self.line_lookup, a:mark_) || 
     \ has_key(self.mark_lookup, a:lineno)
        echoerr "CycleMarks.set_mark attempted to set an existing mark or line!"
        return
    endif
    let pool_index = index(self.mark_pool, a:mark_)
    if pool_index < 0
        echoerr "CycleMarks.set_mark attempted to set an unknown mark: " . a:mark_
        return
    endif
    call remove(self.mark_pool, pool_index)
    let self.mark_lookup[a:lineno] = a:mark_
    let self.line_lookup[a:mark_] = a:lineno
endfunction
"
" FUNCTION: CycleMarks.set_new_mark(int)
" Store the current lineno along with the next
" available mark id (a-z) and return the mark id.
"
function CycleMarks_set_new_mark(lineno) dict
    if has_key(self.mark_lookup, a:lineno) " make sure we only assign a mark once
        return self.mark_lookup[a:lineno]
    endif
    let next_mark = remove(self.mark_pool, 0)
    let self.mark_lookup[a:lineno] = next_mark
    let self.line_lookup[next_mark] = a:lineno
    return next_mark
endfunction
"
" FUNCTION: CycleMarks.sync_marks()
" Sync CycleMarks' cache of marks with the buffers
" actual marks. We do this to account for marks that
" have moved.
"
function CycleMarks_sync_marks() dict
    for mark_ in b:cycle_marks.marks()
        let lineno = s:BufferMarks.position_of(mark_)
        if lineno ==  0
            call b:cycle_marks.delete_mark(mark_)
        else
            call b:cycle_marks.reset_mark(lineno, mark_)
        endif
    endfor
endfunction
"
" FUNCTION: CycleMarks.get_mark(int, func)
" Given a lineno return the MarkObject for the next/prev mark.
"
" Args:
"    lineno: a buffer line number
"    assess: a function pointer to compare line numbers
"
function CycleMarks_get_mark(lineno, assess, sorting) dict
    let marked_lines = Sorted(self.lines(), a:sorting)
    if len(marked_lines) == 0 | return MarkObject("", 0) | endif
    let next_line = -1
    for marked_lineno in marked_lines
        if function(a:assess)(marked_lineno, a:lineno)
            let next_line = marked_lineno
            break
        endif
    endfor
    if next_line == -1 | let next_line = marked_lines[0] | endif
    return self.from_line(next_line)
endfunction
"
" FUNCTION: CycleMarks.next_mark(int)
" Given a lineno return the MarkObject for the next hightest mark line.
"
function CycleMarks_next_mark(lineno) dict
    return self.get_mark(a:lineno, "GreaterThan", 1)
endfunction
"
" FUNCTION: CycleMarks.prev_mark(lineno) 
" Given a lineno return the mark on the next lowest mark line.
"
function CycleMarks_prev_mark(lineno) dict
    return self.get_mark(a:lineno, "LessThan", 0)
endfunction
"
" FUNCTION: CycleMarks.print
" Prints out a list of the internal marks.
"
function CycleMarks_print() dict 
    let marked_lines = Sorted(self.lines(), 1)
    if len(marked_lines) == 0
        echo "CycleMarks has no marks."
        return
    endif
    echo "CycleMarks"
    echo "----------"
    for lineno in marked_lines
        echo printf("%4d : %s", lineno, self.mark_lookup[lineno])
        "echo string(lineno) . " :  " . self.mark_lookup[lineno]
    endfor
endfunction
" 
" FUNCTION: CycleMarks()
" CycleMarks contructor.
" Returns a new CycleMark object.
"
function! CycleMarks()
    let new_object = {
        \ "loaded": 0,
        \ "mark_pool": copy(s:all_marks),
        \ "marks": function("CycleMarks_marks"),
        \ "lines": function("CycleMarks_lines"),
        \ "line_lookup": {},
        \ "mark_lookup": {},
        \ "has_mark": function("CycleMarks_has_mark"),
        \ "delete_mark": function("CycleMarks_delete_mark"),
        \ "from_line": function("CycleMarks_from_line"),
        \ "from_mark": function("CycleMarks_from_mark"),
        \ "reset_mark": function("CycleMarks_reset_mark"),
        \ "sync_marks": function("CycleMarks_sync_marks"),
        \ "set_mark": function("CycleMarks_set_mark"),
        \ "set_new_mark": function("CycleMarks_set_new_mark"),
        \ "get_mark": function("CycleMarks_get_mark"),
        \ "next_mark": function("CycleMarks_next_mark"),
        \ "prev_mark": function("CycleMarks_prev_mark"),
        \ "print": function("CycleMarks_print"),
        \ "reset": function("CycleMarks_reset")}
    return new_object
endfunction

" ============================================================================ 
" CLASS: BufferMarks
" ============================================================================ 
let s:BufferMarks = {}
"
" FUNCTION: BufferMarks.set_mark(str)
" wraps call to vim's `mark' function
" 
function! s:BufferMarks.set_mark(index) 
    exec "mark " . a:index
endfunction

"
" FUNCTION: BufferMarks.delete_mark(str)
" wraps call to vim's `delmark' function
"
function! s:BufferMarks.delete_mark(index)
    exec "delmark " . a:index
endfunction

" 
" FUNCITON: BufferMarks.delete_range(str)
" wraps call to `delmarks abcde`
"
function! s:BufferMarks.delete_range(mark_range)
    exec "delmarks " . a:mark_range
endfunction

" 
" FUNCITON: BufferMarks.delete_all()
" wraps call to `delmarks!' 
"
function! s:BufferMarks.delete_all()
    exec "delmarks!"
endfunction

" 
" FUNCTION: BufferMarks.go_mark(str)
" wraps invocation of `'{mark}`
"
function! s:BufferMarks.go_mark(mark_)
    "echom "go to mark ".a:mark_
    exec "'".a:mark_
endfunction

"
" FUNCTION: BufferMarks.center_cursor()
" wraps invocation of `zz`
"
function! s:BufferMarks.center_cursor()
    exec ":normal zz"
endfunction

"
" FUNCTION: BufferMarks.position_of(str)
" wraps invocation of `getpos("'a")` but just
" returns the line position instead of the full
" list.
"
function! s:BufferMarks.position_of(mark_)
    return getpos("'".a:mark_)[1]
endfunction

"
" Public API
" -------------------------------------------------------------------------

"
" FUNCTION: ShowMarkRange()
" Debugging function. Prints the list of all the marks that 
" CycleMarks has been configured to use.
" 
function! s:ShowMarkRange()
    echo "CycleMarks: " . join(s:all_marks, "")
endfunction

" 
" FUNCTION: ShowBufferVariables()
" Debugging function. Prints the listing of all the internally
" tracked marks.
"
function! s:ShowBufferVariables()
    call b:cycle_marks.print()
endfunction

"
" FUNCTION: ShowNextMark()
" Debugging function. Prints what would be the next mark in
" relation to the current line.
"
function! s:ShowNextMark()
    let mark_ = b:cycle_marks.next_mark(line('.'))
    echo "The next mark is `" . mark_.id . "` at line " . mark_.line
endfunction

" 
" FUNCTION: ShowPrevMark()
" Debugging function. Prints what would be the previous mark in
" relation to the current line.
"
function! s:ShowPrevMark()
    let mark_ = b:cycle_marks.prev_mark(line('.'))
    echo "The prev mark is `" . mark_.id . "` at line " . mark_.line
endfunction

"
" FUNCTION: WhereIsMark(str)
" Prints the location of the given mark.
"
function! s:WhereIsMark(mark_)
    if index(s:all_marks, a:mark_) == -1
        echo "Mark `".a:mark_."` is not a CycleMark."
        return
    endif
    let lineno = s:BufferMarks.position_of(a:mark_)
    echo "mark `".a:mark_."` at line ".lineno
endfunction

" 
" FUNCTION: ShowMark()
" Prints the status of the current lines mark.
"
function! s:ShowMark()
    let mark_ = b:cycle_marks.from_line(line('.'))
    if mark_.exists()
        echo "Line " . mark_.line . " has mark `" . mark_.id . "`."
    else
        echo "Line " . mark_.line . " has no mark."
    endif
endfunction

"
" FUNCTION: InitializeCycleMarks()
" For the current buffer, make sure we have an instance of
" the CycleMarks class. Call this whenever we open/enter/create
" a buffer.
"
function! s:InitializeCycleMarks()
    if !exists('b:cycle_marks') 
        let b:cycle_marks = CycleMarks() 
    endif
endfunction

"
" FUNCTION: LoadExistingMarks()
" Find all existing a-z marks currently in the buffer and 
" load them into CycleMarks. This is only to be called when 
" the buffer is first initialized.
"
function! s:LoadExistingMarks()
    if b:cycle_marks.loaded == 1
        return
    endif
    "echom "Loading marks"
    for mark_ in s:all_marks
        let lineno = s:BufferMarks.position_of(mark_)
        if lineno > 0
            if !b:cycle_marks.has_mark(mark_)
                "echom "loading ".mark_.":".lineno
                call b:cycle_marks.set_mark(lineno, mark_)
            endif
        endif
    endfor
    let b:cycle_marks.loaded = 1
endfunction

" 
" FUNCTION: SetCurrentMark()
" Pull the next available mark and add it to the internal list,
" then set it on the buffer.
"
function! s:SetCurrentMark()
    let next_mark = b:cycle_marks.set_new_mark(line('.'))
    call s:BufferMarks.set_mark(next_mark)
    echo "setting mark: " . next_mark 
endfunction

"
" FUNCTION: RemoveCurrentMark()
"
" Explicity remove the mark from the line the cursor is on.
" This is different than removing a mark that has been already
" removed from the buffer via a line deletion. See ????
"
function! s:RemoveCurrentMark()
    let mark_ = b:cycle_marks.from_line(line('.'))
    if mark_.exists()
        call s:BufferMarks.delete_mark(mark_.id)
        call b:cycle_marks.delete_mark(mark_.id)
    else
        echo "no mark on this line"
    endif
endfunction

"
" FUNCTION: DeleteAllBufferMarks()
"
function! s:DeleteAllBufferMarks()
    call s:BufferMarks.delete_all()
    call b:cycle_marks.reset()
endfunction

"
" FUNCTION: DeleteAllMarks()
"
function! s:DeleteAllMarks()
    call s:BufferMarks.delete_range(g:cycle_marks_mark_range)
    call b:cycle_marks.reset()
endfunction

"
" FUNCTION: GoNextMark()
"
function! s:GoNextMark()
    call b:cycle_marks.sync_marks()
    let mark_ = b:cycle_marks.next_mark(line('.'))
    if !mark_.exists()
        return
    endif
    try
        call s:BufferMarks.go_mark(mark_.id)
        call s:BufferMarks.center_cursor()
    catch
        call b:cycle_marks.delete_mark(mark_.id)
        call s:GoNextMark()
    endtry
endfunction

"
" FUNCTION: GoPrevMark()
"
function! s:GoPrevMark()
    call b:cycle_marks.sync_marks()
    let mark_ = b:cycle_marks.prev_mark(line('.'))
    if !mark_.exists()
        return
    endif
    try
        call s:BufferMarks.go_mark(mark_.id)
        call s:BufferMarks.center_cursor()
    catch
        call b:cycle_marks.delete_mark(mark_.id)
        call s:GoPrevMark()
    endtry
endfunction

"
" Information and Debugging Commands
"
command! -nargs=0 CMShowMarkRange :call s:ShowMarkRange()
command! -nargs=0 CMShowMark :call s:ShowMark()
command! -nargs=0 CMShowNextMark :call s:ShowNextMark()
command! -nargs=0 CMShowPrevMark :call s:ShowPrevMark()
command! -nargs=0 CMShowCycleMarks :call s:ShowBufferVariables()
command! -nargs=1 CMWhereIsMark :call s:WhereIsMark(<f-args>)
" 
" Active Commands
"
command! -nargs=0 CMDeleteMark :call s:RemoveCurrentMark()
command! -nargs=0 CMDeleteAllMarks :call s:DeleteAllMarks() 
command! -nargs=0 CMDeleteAllBufferMarks :call s:DeleteAllBufferMarks()
command! -nargs=0 CMSetMark :call s:SetCurrentMark()
command! -nargs=0 CMGoNextMark :call s:GoNextMark()
command! -nargs=0 CMGoPrevMark :call s:GoPrevMark()

command! -nargs=0 CMSetMarkVariables :call s:InitializeCycleMarks()
command! -nargs=0 CMLoadExistingMarks :call s:LoadExistingMarks()

autocmd BufEnter * :call s:InitializeCycleMarks()
autocmd BufEnter * :call s:LoadExistingMarks()

"
" Default Key Mappings
"
if g:cycle_marks_set_mark_default == 1
    noremap <Leader>m :CMSetMark<CR>
endif

